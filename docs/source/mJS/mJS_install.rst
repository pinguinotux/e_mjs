mJS como submodulo del proyecto
===============================

`Repositorio como submodulo <https://git-scm.com/book/es/v1/Las-herramientas-de-Git-Subm%C3%B3dulos>`_

En el proyecto, en el directorio que se desee ejecuatar el siguiente comando:

.. code-block:: sh

    git submodule add https://github.com/cesanta/mjs.git mJS/

El anterior comando clonará el proyecto mjs en mJS como submodulo del proyecto 
en cuestión.
